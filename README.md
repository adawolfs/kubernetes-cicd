# Kubernetes CICD


# Lest use a Kubernetes playground at Katacoda
https://katacoda.com/courses/kubernetes/playground

## Deploy gitlab-runner into kubernetes
For this I'll use helm
https://helm.sh/docs/intro/install/

To install helm I'll use the easy and probably insecure way because I dont care about security :)
```
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh 
```
Prepare helm
```
helm init
helm repo add gitlab https://charts.gitlab.io
```

Install runner
```
helm install --namespace gitlab -f values.yaml gitlab-runner gitlab/gitlab-runner
```
